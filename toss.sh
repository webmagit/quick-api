: '

The MIT License (MIT)

Copyright (c) 2015 Cameron Oxley

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'

#!/bin/bash

# check for errors and exit
set -e

# set default global variables (can be overridden by config)
collection=''
env=''
global=''
webhook=''
properties=''
url=''
additional_args=''

# config environment
config_file=''
config_file_secured=''

# local vars
strip_colour=''
summarize=true
verbose=true
send_webhook=false

# readonly vars
readonly overrideable_vars=( '^env=' '^collection=' '^webhook=' '^global=' '^additional_args=' )
readonly config_filter='^#|^[^ ]*=[^;]*'

usage="$(basename "$0") -- Runs a Newman test script and outputs the summary to a Slack webhook

# Usage #

    -h            		show this help text
    -r [file]     		run a bash configuration environment (overwrites passed args)
    -c [file]     		postman collection to run
    -u [url]      		postman collection url to run
    -e [file]     		postman environment to reference
    -w [url]      		slack webhook to call
    -g [file]     		postman global to reference
    -a [command]  		additional Newman command ex: -a \"-H report.html -t junit.xml\"
    -C            		disable colorized output to screen, use with -S or -V
    -N            		disable webhook call
    -p [properties]  		properties file to read and push to slack 
    --bambooResultsURL=[url]	build result URL to be hyperlinked to Project field
    
Where one of: -c [file] or -u [url] or -p [properties] is required"

optspec=":hSNVvCc:e:g:w:a:r:u:p:-:"

# pass input values
while getopts $optspec option; do
    case "$option" in
	-)
		case "${OPTARG}" in
	        bambooResultsURL)
                    bambooResultsURL="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    echo "Parsing option: '--${OPTARG}', value: '${bambooResultsURL}'" >&2;
                    ;;
                bambooResultsURL=*)
                    bambooResultsURL=${OPTARG#*=}
                    opt=${OPTARG%=$bambooResultsURL}
                    echo "Parsing option: '--${opt}', value: '${bambooResultsURL}'" >&2
                    ;;
                *)
                        echo "Unknown option --${OPTARG}" >&2
                    ;;
		esac;;

        h)  echo "$usage"
            exit
            ;;
        c)  collection="$OPTARG"
            ;;
        u)  url="$OPTARG"
            ;;
        r)  config_file="$OPTARG"
			config_file_secured="/tmp/$config_file"
            ;;
        e)  env="$OPTARG"
            ;;
        g)  global="$OPTARG"
            ;;
        w)  webhook="$OPTARG"
		send_webhook=true
            ;;
        a)  additional_args="$OPTARG"
            ;;
        S)  summarize=true
            ;;
        N)  send_webhook=false
            summarize=true
            ;;
        V)  verbose=true
            ;;
        C)  strip_colour='-C '
            ;;
        p)  properties="$OPTARG"
            ;;
        :)  printf "missing argument for -%s\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
       \?)  printf "illegal option: -%s\n" "$OPTARG" >&2
            echo "$usage" >&2
            exit 1
            ;;
    esac
done

# remove excess options
shift "$((OPTIND-1))"

# fetches the source config
function loadConfig () {

	if [ -f "$config_file" ] ; then

		# check if the file contains bash commands and other junk
		if egrep -q -v "$config_filter" "$config_file"; then

			echo "Cleaning config file" >&2

			# filter to a clean file
			egrep "$config_filter" "$config_file" > "$config_file_secured"
			config_file="$config_file_secured"
		fi

		# load the file and only override the vars we accept
		for i in "${overrideable_vars[@]}"
		do
			# bash 3.2 fix for source process substitution
			source /dev/stdin <<<"$(grep "$i" "$config_file")"
		done

	else
		printf "\nError: Could not locate file '$config_file'" >&2
		exit 1
	fi
}

# read and store properties
function publishAlertsToSlack () {

	defaultPropertiesFile="./build-result.properties"
	
	if [ -f "$properties" ] ; then 

		echo "Reading properties from: $properties"
		file=$properties

	elif [ -f "$defaultPropertiesFile" ] ; then

	  	echo "Reading DEFAULT properties from: $defaultPropertiesFile"
		file=$defaultPropertiesFile

	else 
		echo "No properties file defined"
            	echo "$usage" >&2
		exit 1
	fi
	
	slackNotification=0 # by default unless enabled via incoming properties

	  while IFS='=' read -r key value
	  do
	    key=$(echo $key | tr '.' '_')
		test -z "$key" && continue # skipping empty lines
	    eval "${key}='${value}'"
	  done < "$file"

	# not posting to slack if not meant to
	# ex: non-critical test failures

	if [ $slackNotification != 1 ] ; then

		echo "slackNotification = " $slackNotification	
		echo "Hence Ignoring Slack Request"
		exit 0

	fi

	colon=":"
	
	channel="test"
	username="health-alert-bot"
	icon_emoji=$colon$icon_emoji$colon # smile

	# fields
	projectTitle="Project" 

	if [ $bambooResultsURL ] ; then
		project="<"$bambooResultsURL"|"$project">"
	fi

	environmentTitle="Environment" 

        # post to slack
        slackPostStatus=$(curl -sw '%{http_code}' -X POST -d '
	
		{
			"channel": "'"$channel"'", 
			"username": "'"$username"'",
			"icon_emoji": "'"$icon_emoji"'",
			"text": "'"$message"'",
			"attachments": [{
						"title": "'"$title"'",
						"title_link": "'"$title_link"'",
						"text": "'"$text"'",
						"image_url": "'"$image_url"'",
						"color": "'"$color"'", 
						"fields": [
								{
									"title": "'"$projectTitle"'",
									"value": "'"$project"'",
									"short": true
								},
								{
									"title": "'"$environmentTitle"'",
									"value": "'"$environment"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle1"'",
									"value": "'"$fieldValue1"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle2"'",
									"value": "'"$fieldValue2"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle3"'",
									"value": "'"$fieldValue3"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle4"'",
									"value": "'"$fieldValue4"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle5"'",
									"value": "'"$fieldValue5"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle6"'",
									"value": "'"$fieldValue6"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle7"'",
									"value": "'"$fieldValue7"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle8"'",
									"value": "'"$fieldValue8"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle9"'",
									"value": "'"$fieldValue9"'",
									"short": true
								},
								{
									"title": "'"$fieldTitle10"'",
									"value": "'"$fieldValue10"'",
									"short": true
								}

						]

			}]

		}

	' $webhook)


	if [ "$slackPostStatus" == "ok200" ] ; then
		echo "Post to Slack: SUCCESS"
	else 
		echo "Post to Slack: FAILED"
	fi

}

# preprend newman args to commands
function prependNewmanArgs () {

	if [ -n "$url" ] ; then
        url="-u $url"
    fi

    if [ -n "$global" ] ; then
        global="-g $global"
    fi

    if [ -n "$env" ] ; then
        env="-e $env"
    fi

    if [ -n "$collection" ] ; then
        collection="-c $collection"
    fi
}

# process the script
function init () {

	# prepend newman arguments to vars
	prependNewmanArgs

    # call newman
    local output=$(newman $collection $env $url $global $additional_args $strip_colour)

    # output verbose file
    if [ "$verbose" = true ] ; then
        echo "$output"
    fi

    # only get summary lines
    local summary=$(echo "$output" | awk '/^Summary:/,/^%Total/ {print}')

    # output summary if set
    if [ "$summarize" = true ] ; then
        echo "$summary"
    fi

    # call webhook
    if [ "$send_webhook" = true ] ; then

        # remove colour from file
        local noColor=$(echo "$summary" | perl -pe 's/\x1b\[[0-9;]*m//g')
		# substitution for \ in file path so curl likes it
        noColor=$(echo "$noColor" | perl -pe 's/\\/->/g')
		
		# debug what's sent
		#echo $noColor

        # post to slack
        curl -X POST --data-urlencode 'payload={"text": "```'"$noColor"'```"}' $webhook
    fi
}

# check if we need to load the config
 if [ -n "$config_file" ] ; then
 	loadConfig
 fi

# validate required args
if [ -z "$collection" ] && [ -z "$url" ] && [ -z "$properties" ]  ; then
	printf "\nError: One of -c [file] or -u [url] or -p [properties-file] is required\n\n" >&2
	echo "$usage" >&2
    exit 1
elif [ -n "$collection" ] && [ -n "$url" ] ; then
	# check against both -c and -u being called
	printf "\nError: Only one of -c [file] or -u [url] is required\n\n" >&2
    echo "$usage" >&2
    exit 1
fi

# if verbose and summary are set, disable summary
if [ "$summarize" = true ] && [ "$verbose" = true ] ; then
    summarize=false
fi

if [ -n "$properties" ] && [ -z "$webhook" ] ; then
	printf "\nHint: Specify a webhook. Ex: -w <hook-url>\n\n" >&2
	echo "$usage" >&2
	exit 1
fi


if [ -n "$properties" ] ; then

	publishAlertsToSlack
	

else

	# initialize the usual
	init

fi
