## Setup

```bash
npm install -g newman
npm install
npm start
```

Our demo api is running now.

## Execute tests

Since the api is only for demo cases, after each run (with or without data file) you need to restart the api so the objects array is emptied 

### Without a data file

Executing without a datafile means you run your testcollection once.

```bash
newman -c test-collection.json
```

### With a data file

```bash
newman -c test-collection-with-data.json -d test-data.json
```

### With package.json


`npm start` 
`rs` within console for fast restart
```bash
npm test
``` 
for the normal tests and 
```bash
npm run testWithData
```
for the iterated tests with different datasets.
